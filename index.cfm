<cfsilent>
    <cfparam name="url.sortBy" default="datelastmodified" />
    <cfparam name="url.sound" default="true" />
</cfsilent>
<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="/cftoilet/css/bootstrap.min.css">

    <style>
        body {
            margin-top: 80px;
        }
        #content, #nav-bar{
            width: 90%;
        }
        .file-content {
            overflow-x: scroll;
        }
        .btn-group{
            padding: 0px 5px;
        }
        .btn {
            width: 100px;
            letter-spacing: 2px;
        }
        .hotkeys {
            color: #7799CC;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-inner" style="border-bottom: 1px solid #ddd;">
            <div id="nav-bar" class="container row-fluid" style="padding: 10px 0px;">
                <div class="brand span2" style="font-size: 36px;"><span style="font-weight: 100;">CF</span> Toilet</div>
                <div class="btn-group span1"><button id="btnFlush" class="btn btn-inverse"><span class="hotkeys">F</span>lush</button></div>
                <div class="btn-group span1">
                    <button class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">
                        Sort
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a id="byTitle" class="opt-sort sort-by" data-sort="name" href="##"><span class="hotkeys">T</span>itle</a></li>
                        <li><a id="byDate" class="opt-sort sort-by" data-sort="datelastmodified" href="##"><span class="hotkeys">D</span>ate</a></li>
                    </ul>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div id="content" class="container">
        <cfoutput>
            <cfdirectory name="folderContents" action="list" directory="#getTempDirectory()#" type="file" mode="777" sort="#url.sortBy# DESC" />

            <cfloop query="folderContents">
                <cffile action="read" file="#getTempDirectory() & folderContents.name#" variable="contents" />

                <div class="file-content well" contenteditable="true">
                    <h4>#folderContents.name# | <span style="font-weight: 100;">#folderContents.datelastmodified#</span> </h4>
                    #contents#
                </div>
            </cfloop>
        </cfoutput>

        <div class="flush-wav"></div>
    </div>

    <script type="text/javascript" src="/cftoilet/js/jquery.js"></script>
    <script type="text/javascript" src="/cftoilet/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        !(function(){
            var sound = "<cfoutput>#url.sound#</cfoutput>";
            var locationObj = window.location;
            eval(sound) && eval(localStorage.getItem("flushed")) && playFlush();

            $(document)
                .on("keyup", function(e) {initHotkeys(e.keyCode)})
                .on("click", "#btnFlush", flushDumps)
                .on("click", ".opt-sort", sortContents);

            function sortContents() {
                var sort = $(this).data("sort");
                var params = $.param({
                    sound: sound
                    ,sortBy: sort
                });

                locationObj.href = "http://" + locationObj.host + locationObj.pathname + "?" + params;
            }

            function initHotkeys(keyCode) {
                keyCode === 70 && flushDumps();
                keyCode === 84 && $("#byTitle").click();
                keyCode === 68 && $("#byDate").click();
            }

            function flushDumps() {
                $.ajax({url: "http://10.11.12.13/cftoilet/empty_dumps.cfm"}).done(function () {
                    localStorage.setItem("flushed", true);
                    location.reload();
                });
            }

            function playFlush() {
                localStorage.setItem("flushed", false);
                $(".flush-wav").html("<embed src='flush.wav' hidden=true autostart=true loop=false>");
            }
        })($, window);
    </script>
</body>
</html>