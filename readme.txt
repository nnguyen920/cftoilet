Where dumps go.

======================================================
===========   How to dump   ==========================
======================================================

<cfdump output="[filename]" format="html" />

Dumps to coldfusion temp directory.  Use getTempDirectory() to see path.

======================================================
===========   Helper Function   ======================
======================================================

<cffunction name="dump" returnType="void" access="public" output="false" hint="">
    <cfargument name="data" type="any" required="true" />
    <cfargument name="fileName" type="string" default="dump" />

    <cfdump var="#arguments.data#" output="#arguments.fileName#_#getTickCount()#.htm" format="html" />
</cffunction>

======================================================
===========   Sublime Text 2 Snippet   ===============
======================================================

<snippet>
    <content><![CDATA[<cfdump var='#$1#' output='$1_#getTickCount()#'  format='html' />$0]]></content>
    <tabTrigger>cfdump</tabTrigger>
    <scope>text.html.cfm</scope>
    <description>cfdump - to file</description>
</snippet>

======================================================
===========   Sublime Text 2 Key bindings   ==========
======================================================

{
    "keys": ["alt+ctrl+d"], "command": "insert_snippet",
    "args": { "contents": "<cfdump var='#$1#' output='$1_#getTickCount()#'  format='html' />" },
    "context":
    [
        {"key": "selector", "operator": "equal", "operand": "text.html.cfm"},
        {"key": "selector", "operator": "not_equal", "operand": "source.cfscript.embedded.cfml" }
    ]
}